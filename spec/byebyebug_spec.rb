require_relative '../lib/byebyebug'

RSpec.describe ByeByeBug::Checker do
  context 'with a good file' do
    checker = ByeByeBug::Checker.new('spec/examples/good.rb')
    checker.check

    describe '#check' do
      it 'should set error = false' do
        expect(checker.error).to eq(false)
      end
    end

    describe '#report' do
      it 'should print the "No bad files" message' do
        expect(STDOUT).to receive(:puts).with('No bad files')
        checker.report
      end
    end
  end

  context 'with a bad file' do
    checker = ByeByeBug::Checker.new('spec/examples/bad.rb')
    checker.check

    describe '#check' do
      it 'should set error = true' do     
        expect(checker.error).to eq(true)
      end

      it 'should have correct bad_files hash' do      
        expect(checker.bad_files).to eq({'spec/examples/bad.rb' => [3, 6]})
      end
    end

    describe '#report' do
      it 'should print the "Bad Files:" report' do
        expect(STDOUT).to receive(:puts).with('Bad files:')
        expect(STDOUT).to receive(:puts).with('spec/examples/bad.rb on number(s): 3,6')
        checker.report
      end
    end
  end
end