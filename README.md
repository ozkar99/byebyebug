# byebyebug

This is a simple gem providing a single class, for checking if there are stranded byebug|debug|binding.pry on files.  

It also comes with a bundled script, that checks given glob.  

## instalation

`gem install byebyebug` yep, thats it.  

you can find more info on https://rubygems.org/gems/byebyebug

## usage

### script

simply run `byebyebug` it will check current directory and descendats for `*.rb` 
it will return `0` if succesful and `1` on error (useful for ci systems).  
and also display a report with the file and line numbers of the aggresion.  

### ByeByeBug::Checker
theres also a class, if you want to make a custom script that supports more fancy options like passing directories or single files.  

initialize with a string glob (or use the default), use `#check` to run check on the glob, after that you can do `#report` to print the report or `#error` to check if an error is present.  
