#!/usr/bin/env ruby
# Search for byebug|debug|binding.pry on files

module ByeByeBug
  class Checker
    COMMENT_REGEX = /^#.*/
    BYEBUG_REGEX = /.*(byebug|binding.pry)\s+.*/ 

    attr_reader :glob, :bad_files

    def initialize(glob = "./**/*.rb")
      @glob = glob
      @bad_files = {} # 'path' => [list of numbers in file]
    end

    def check      
      Dir[glob].each do |path|
        File.open(path, 'r').each_line.with_index do |line, line_number|
          next if line =~ COMMENT_REGEX # ignore if its a comment          

          # match
          if line =~ BYEBUG_REGEX          
            @bad_files[path] ||= []
            @bad_files[path] << line_number + 1 # starts at 0
          end
        end
      end
    end

    def report
      if error
        puts 'Bad files:'
        @bad_files.each do |path, line_numbers|
          puts "#{path} on number(s): #{line_numbers.join(',')}"
        end
      else
        puts 'No bad files'
      end
    end

    def error
      !@bad_files.empty?
    end
  end
end
