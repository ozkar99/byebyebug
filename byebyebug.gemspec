Gem::Specification.new do |s|
  s.name        = 'byebyebug'
  s.version     = '0.3.1'
  s.date        = '2018-02-20'
  s.summary     = 'A gem/script that displays files with stranded byebug|debug|binding.pry directives'
  s.authors     = ['Oscar Moreno Garza']
  s.email       = 'oscarmg99@gmail.com'
  s.required_ruby_version = '~> 2.3'
  s.files       = ['lib/byebyebug.rb']
  s.homepage    = 'https://gitlab.com/ozkar99/byebyebug'
  s.license     = 'BSD-2-Clause'

  s.executables   = ['byebyebug']

  s.add_development_dependency 'rspec', '~> 3.7'
  s.add_development_dependency 'byebug', '~> 10.0'
end
